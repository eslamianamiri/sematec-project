<?php
    session_start() ;
    /*if($_SESSION["userName"] == null){
        header("Refresh:0;url=login.php") ;
        exit() ;
    }*/
    if($_COOKIE['myCookie'] == null){
        header("Refresh:0;url=login.php") ;
        exit() ;
    }
    $erorr = "" ;
    $contryResult = "" ;
    if(!empty($_POST["submit"])){
        $apiUrl = "https://restcountries.eu/rest/v2/name/".$_POST["country"] ;
        $countryFile = file_get_contents($apiUrl) ;
        $json = json_decode($countryFile , true) ;
        if(empty($json[0])){
            $erorr = "country not found" ;
        }else{
            $contryPopulation = $json[0]['population'] ;
            $contryLangs = $json[0]['languages'] ;
            $contryCapital = $json[0]['capital'] ;
            $contryResult = $_POST["country"]." population is : " . $contryPopulation ."<br>" ."capital is: ". $contryCapital  ;
        }
    }
?>
<!DOCTYPE html>
<html lang="fa" dir="rtl">
<title>پروژه های php</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
    .w3-bar,h1,button {font-family: "Montserrat", sans-serif}
    .fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>
<?php include_once "header.php" ?>
<form method="post" action="?">
    <input type="text" name="country" placeholder="enter the country">
    <input type="submit" name="submit" value="submit_form">
</form>
<?= $erorr ?>
<?= $contryResult ?>
<?php include_once "footer.php" ?>
</body>
</html>