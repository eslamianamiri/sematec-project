<?php
//session_start() ;
include_once "methods.php" ;
$order = $_REQUEST["order"] ;
if ($order == "login"){
    $userName =cleanWord( $_POST["username"]) ;
    $password = cleanWord($_POST["password"]) ;
    $userInformation = file_get_contents("registers") ;
    $arr = json_decode($userInformation , true) ;
    if ($userName == $arr['username'] && $password == $arr['password']){
        header("Refresh:0;url=rest.php") ;
        //$_SESSION["userName"] = $userName;
        setcookie("myCookie" , $userName , time()+3600 , "/") ;
        $message = "شما با موفقیت وارد شدید" ;
        logEvent("user entered ($userName $password)");
    }else{
        header("Refresh:4;url=login.php") ;
        $message = "نام کاربری یا گذر واژه اشتباه است" ;
    }
} ;
?>
<!DOCTYPE html>
<html lang="fa" dir="rtl">
<title>پروژه های php</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
    .w3-bar,h1,button {font-family: "Montserrat", sans-serif}
    .fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>
    <?php include "header.php" ?>
    <b><?php echo $message ?></b>
    <?php include "footer.php" ?>
</body>
</html>
