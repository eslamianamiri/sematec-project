<?php
include_once "methods.php" ;
$userError = "" ;
$passwordEror = "" ;
$emailError = "" ;
$webSiteError = "" ;
$message = "" ;
if(!empty($_REQUEST['submit'])){
    $userName =cleanWord( $_POST["username"]) ;
    $password = cleanWord($_POST["password"]) ;
    $order = $_POST["order"] ;
    $email = $_POST["email"] ;
    $webSite = $_POST["website"] ;

    if( empty($userName)){
        $userError = "نام کاربری اجباری است" ;
    }
    if(empty($password)){
        $passwordEror = "رمز عبور اجباری است" ;
    }
    if( !filter_var($email, FILTER_VALIDATE_EMAIL)){
        $emailError = "فرمت ایمیل اشتباه است" ;
    }
    if( !filter_var($webSite, FILTER_VALIDATE_URL)){
        $webSiteError = "فرمت وب سایت اشتباه است" ;
    }
    if( empty($userNameError) && empty($passwordEror) && empty($emailError) && empty($webSiteError)){
        $outPut = array("username" => $userName , "password" => $password) ;
        $jSon = json_encode($outPut) ;
        file_put_contents('registers' , $jSon) ;
        //header("Refresh:4;url=register.php") ;
        $message = "ثبت نام شما با موفقیت انجام شد" ;
        logEvent("new user registered ($userName $password)") ;
    }

} ;
?>
<!DOCTYPE html>
<html lang="fa" dir="rtl">
<title>پروژه های php</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
    .w3-bar,h1,button {font-family: "Montserrat", sans-serif}
    .fa-anchor,.fa-coffee {font-size:200px}
</style>
<body>
    <?php include "header.php" ?>
    <form method="post" action="?">
        <input type="hidden" name="order" value="register">
        <input type="text" name="username" placeholder="Enter your username">
        <span><?php echo $userError ?></span>
        <input type="password" name="password" placeholder="Enter your password">
        <span><?=$passwordEror?></span>
        <input type="text" placeholder="enter email" name="email">
        <span><?=$emailError?></span>
        <input type="text" placeholder="enter website" name="website">
        <span><?=$webSiteError?></span>
        <input type="submit" name="submit" value="ثبت درخواست">
    </form>
    <?= $message ?>
    <?php include "footer.php" ?>
</body>
</html>