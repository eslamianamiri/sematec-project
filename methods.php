<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 31/05/2018
 * Time: 05:06 PM
 */
    function cleanWord($word)
    {
        $outPut = trim($word);
        $outPut = stripcslashes($outPut);
        $outPut = str_replace("۱", "1", $outPut);
        $outPut = str_replace("۲", "2", $outPut);
        $outPut = str_replace("۳", "3", $outPut);
        $outPut = str_replace("۴", "4", $outPut);
        return $outPut;
    }

    function logEvent($msg)
    {
        date_default_timezone_set('Asia/Tehran');
        $date = date('Y-m-d H:i:s', time());
        $userIP = $_SERVER['REMOTE_ADDR'];
        file_put_contents("log",
            $date . " " .
            $userIP . " " .
            $msg . "\n",
            FILE_APPEND);
    }

?>