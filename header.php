﻿
<!-- Navbar -->
<div class="w3-top">
    <div class="w3-bar w3-red w3-card w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-red" href="javascript:void(0);" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
        <a href="index.php" target="_blank" class="w3-bar-item w3-button w3-padding-large w3-white">صفحه نخست</a>
        <a href="arrays.php" target="_blank"  class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">آرایه ها</a>
        <a href="register.php" target="_blank" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">ثبت نام</a>
        <a href="login.php" target="_blank" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">ورود اعضا</a>
        <a href="logout.php" target="_blank" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">خروج</a>
        <a href="functions.php" target="_blank" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">توابع و حلقه های شرطی</a>
        <a href="https://gitlab.com/eslamianamiri/sematec-project" target="_blank" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">پروژه در Gitlab</a>
        <a href="rest.php" target="_blank" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white">سرویس رست</a>
    </div>

    <!-- Navbar on small screens -->
    <div id="navDemo" class="w3-bar-block w3-white w3-hide w3-hide-large w3-hide-medium w3-large">
        <a href="arrays.php" target="_blank" class="w3-bar-item w3-button w3-padding-large">آرایه ها</a>
        <a href="register.php" target="_blank" class="w3-bar-item w3-button w3-padding-large">ثبت نام</a>
        <a href="login.php" target="_blank" class="w3-bar-item w3-button w3-padding-large">ورود اعضا</a>
        <a href="logout.php" target="_blank" class="w3-bar-item w3-button w3-padding-large">خروج</a>
        <a href="functions.php" target="_blank" class="w3-bar-item w3-button w3-padding-large">توابع و حلقه های شرطی</a>
        <a href="https://gitlab.com/eslamianamiri/sematec-project" target="_blank" class="w3-bar-item w3-button w3-padding-large">پروژه در Gitlab</a>
        <a href="rest.php" target="_blank" class="w3-bar-item w3-button w3-padding-large">سرویس رست</a>
    </div>
</div>

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
    <h1 class="w3-margin w3-jumbo">پروژه های PHP</h1>
    <p class="w3-xlarge"><?php  echo "امیر اسلامیان"  ?></p>
</header>
